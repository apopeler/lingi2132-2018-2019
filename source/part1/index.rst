.. _part1:


*************************************************************************************************
Partie 1 | Lexical Analysis
*************************************************************************************************

Question Title proposed by Group XX, Members
==========================================================


Sub question
""""""""""""""


Some bullets

* bullet1 :math:`x^2`
* bullet2 
* `Wikipedia <http://www.wikipedia.org>`_ 


Some code in java:

.. code-block:: java


        FList<Integer> list = FList.nil();

        for (int i = 0; i < 10; i++) {
            list = list.cons(i);
        }

        list = list.map(i -> i+1);
        // will print 1,2,...,11
        for (Integer i: list) {
            System.out.println(i);
        }

        list = list.filter(i -> i%2 == 0);
        // will print 2,4,6,...,10
        for (Integer i: list) {
            System.out.println(i);
        }



Image example

    .. image:: dfs.svg
        :scale: 50
        :width: 250
        :alt: DFS